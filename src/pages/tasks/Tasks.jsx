import './Tasks.css';
import { Task } from '../../components';

export const Tasks = () => (
	<div className="Tasks">
		<h1>Example To-do List</h1>
		<Task title="Task 1" />
		<Task title="Task 2" />
		<Task title="Task 3" complete />
	</div>
);
