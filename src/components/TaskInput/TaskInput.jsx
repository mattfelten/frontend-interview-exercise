import css from './TaskInput.module.css';

export const TaskInput = () => (
	<input
		className={css.input}
		placeholder="What should I do today?"
	/>
);
