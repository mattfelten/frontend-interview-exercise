# Frontend Interview Exercise

This exercise is a test drive to see how you work on a semi-real project. An hour is not enough time to complete the entire project, and that's ok. We're looking at what you do with the hour & how you spend your time. **Please [set up the project](#setting-up-project) ahead of time.**

This exercise is **open book** &ndash; if you need to reference something, feel free to ask questions, or consult online documentation. We're not assessing how much you keep stored in your brain but how you think through problems to get work done.

## Description
A basic todo list app. Because the world needs another one.

[Design Spec in Figma](https://www.figma.com/file/n7Bgxf74AyQVPcqt76Y9Th/Frontend-Engineer-Interview-Exercise?node-id=0%3A1)

## Tasks
This is a list of work to be done to ship this app. Choose a task or two to work on with the time you have available.
- [ ] Load data from `/api/data.json` using [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) or a library of your choosing and populate the UI with real tasks.
	- Add at least 1 passing test
- [ ] Create a new route `/completed` that displays only completed tasks with the date completed. 
	- Add at least 1 passing test
- [ ] Add `TaskInput` to landing page and make it add new tasks to the task list.
	- Add at least 1 passing test
- [ ] Udate `Task` component to show completed date for completed items

## System Requirements
- Node LTS (18). Shouldn't affect the exercise if on an older Node version but good to be safe.

> **NOTE:**  Feel free to manage your Node installation in whatever way you prefer whether that be a native installation, using a version manager like [nvm](https://github.com/nvm-sh/nvm) or [n](https://github.com/tj/n), or using a Docker-based installation.

## Setting up project

1. Clone this repository  
`git clone https://gitlab.com/missioncloud-public/frontend-interview-exercise.git`
1. ~~Install dependencies~~ Dependencies are installed locally to save time  
~~`npm i`~~  

1. Start project locally.  The application should be accessible at http://localhost:5173  
`npm start`
1. Run the example provided test and observe that it fails  
`npm test`
