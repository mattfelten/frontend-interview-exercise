import { render, screen } from '@testing-library/react';
import { Tasks } from './Tasks';

// Feel free to delete. Just making sure testing works
it('should show the title of the page', () => {
	render(<Tasks />);
	expect(
		screen.getByText('Example To-do List')
	).toBeInTheDocument();
});

it('should prompt the user to specify a new task', () => {
	render(<Tasks />);
	const taskInput = screen.getByPlaceholderText(
		'What should I do today?'
	);
	expect(taskInput).toBeInTheDocument();
});
