import clsx from 'clsx';
import css from './Task.module.css';
import { ReactComponent as Check } from './check.svg';
import { ReactComponent as Circle } from './circle.svg';

export const Task = ({ title, complete }) => {
	const classes = clsx(css.task, { [css.completed]: complete });
	const Icon = complete ? Check : Circle;
	return (
		<div className={classes}>
			<Icon className={css.icon} />
			<span>{title}</span>
		</div>
	);
};
